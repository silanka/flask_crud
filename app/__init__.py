from flask import Flask, render_template, request, redirect, flash, url_for
from wtforms import Form, StringField, validators, IntegerField, HiddenField
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc

app = Flask(__name__)
app.config["SECRET_KEY"] = "HFDFHJDbdcsd76"
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/alchemy'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = False

db = SQLAlchemy(app)


class Guest(db.Model):
    '''
    this creates the guest table in the database
    '''
    id = db.Column(db.Integer(), primary_key=True)
    firstname = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    email = db.Column(db.String(80), unique=True)
    tel = db.Column(db.BigInteger())

    def __init__(self, firstname, lastname, email, tel):

        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.tel = tel


class RegisterGuest(Form):
    """this creates the guest registration form"""
    fname = StringField('First Name (50 char max)', [validators.required(),
                        validators.Length(min=3, max=50)])
    lname = StringField('Last Name (50 char max)', [validators.required(),
                        validators.Length(min=3, max=50)])
    email = StringField('Email', [validators.required(), validators.Email()])
    tel = IntegerField('Phone Number (20 char max)',
                    [validators.required(), validators.Length(min=4, max=20)])
    id = HiddenField()


@app.route('/', methods=['GET', 'POST'])
def index():
    form = RegisterGuest()
    # this adds new guest to the table
    if request.method == "POST" and form.validate:
        rec = request.form
        fname = rec['fname']
        lname = rec['lname']
        email = rec['email']
        tel = rec['tel']
        if rec['id'] != "":
            edit_id = rec['id']
            edit = Guest.query.filter_by(id=edit_id).first()
            edit.firstname = fname
            edit.lastname = lname
            edit.email = email
            edit.tel = tel
            message = f"{fname} {lname} has been updated in the guest list"
        else:
            data = Guest(fname, lname, email, tel)
            db.session.add(data)
            message = f"{fname} {lname} has been added to guest list"
        try:
            db.session.commit()
        except exc.IntegrityError:
            db.session.rollback()
            flash("email already Exists")
            return redirect(request.url)
        except exc.OperationalError:
            db.session.rollback()
            flash("Numeric values expected in Telephone Field")
            return redirect(request.url)
        except exc.DataError:
            db.session.rollback()
            flash("""
            values out of range. please stick to the
             recommended number of characters
                """)
            return redirect(request.url)
        else:
            flash(message)
            return redirect(request.url)
    # this function select data from the database
    db.create_all()
    data = Guest.query.all()
    return render_template('index.html', form=form, guests=data)


@app.route("/delete/<del_id>")
def delete(del_id):
    delete_item = Guest.query.filter_by(id=del_id).first()
    db.session.delete(delete_item)
    db.session.commit()
    return redirect(url_for('index'))
